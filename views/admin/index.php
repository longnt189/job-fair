<?php
session_start();
if (empty($_SESSION['username'])) {
    header('location: login.php');
}
require '../../config.php';
include ('../../models/db.php');
include ('../../models/dashboard.php');
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Starter</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="plugins/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">

  <link rel="stylesheet" href="dist/css/skins/skin-purple.min.css">
  <link rel="stylesheet" href="dist/css/style.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-purple sidebar-mini">
  <div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

      <!-- Logo -->
      <a href="" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>D</b>UE</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Jobfair 2018</b></span>
      </a>

      <!-- Header Navbar -->
      <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <!-- The user image in the navbar-->
                <img src="dist/img/Logo_Kinh_tế_Đà_Nẵng.jpg" class="user-image" alt="User Image">
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span class="hidden-xs" style="text-transform: uppercase">Trường Đại học Kinh tế - Đại học Đà Nẵng</span>
              </a>

            </li>
          </ul>
        </div>
      </nav>
    </header>

    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="dist/img/Logo_Kinh_tế_Đà_Nẵng.jpg" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p>Admin Jobfair</p>
            <!-- Status -->
            <a href="../../models/logout.php" style="font-size: 14px">Đăng xuất</a>
          </div>
        </div>


        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">Quản lý</li>
          <!-- Optionally, you can add icons to the links -->
          <li class="active"><a href="#"><i class="fa fa-link"></i> <span>Tổng quan</span></a></li>
        </ul>
        <!-- /.sidebar-menu -->
      </section>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
            Danh sách đơn đăng ký
          <small>Ngày hội việc làm Jobfair 2018</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Ngày hội việc làm</a></li>
          <li class="active">Danh sách</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content container-fluid">

          <div class="row">
              <div class="panel panel-primary filterable">
                  <div class="panel-heading">
                      <h3 class="panel-title">JOBFAIR</h3>
                      <div class="pull-right">
                          <button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Lọc và tìm kiếm</button>
                      </div>
                  </div>
                  <div class="table-responsive">
                      <table class="table table-bordred table-striped">
                          <thead>
                          <tr class="filters">
                              <th><input type="text" class="form-control" placeholder="No." style="width: 20px;" disabled></th>
                              <th><input type="text" class="form-control" placeholder="Họ và tên" style="width: 170px;" disabled></th>
                              <th><input type="text" class="form-control" placeholder="Ngày sinh" style="width: 90px;" disabled></th>
                              <th><input type="text" class="form-control" placeholder="Email" disabled></th>
                              <th><input type="text" class="form-control" placeholder="Điện thoại" disabled></th>
                              <th><input type="text" class="form-control" style="width: 110px;" placeholder="Trường" disabled></th>
                              <th><input type="text" class="form-control" style="width: 130px;" placeholder="Chuyên ngành" disabled></th>
                              <th><input type="text" class="form-control" style="width: 75px;" placeholder="Sinh viên" disabled></th>
                              <th><input type="text" class="form-control" style="width: 100px;" placeholder="Hình thức" disabled></th>
                              <th><input type="text" class="form-control" style="width: 270px;" placeholder="Lĩnh vực quan tâm" disabled></th>
                              <th><input type="text" class="form-control" placeholder="File CV" disabled></th>
                          </tr>
                          </thead>
                          <tbody>
                          <?php loadCV(); ?>
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
      <!-- To the right -->
      <div class="pull-right hidden-xs">

      </div>
      <!-- Default to the left -->
      <strong>06/2018 - <a href="#">Trường Đại học Kinh tế - Đại học Đà Nẵng</a>.</strong>
    </footer>


    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

  <!-- REQUIRED JS SCRIPTS -->

  <!-- jQuery 3.1.1 -->
  <script src="plugins/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="plugins/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- AdminLTE App -->
  <script src="dist/js/adminlte.min.js"></script>
  <script src="dist/js/main.js"></script>

</body>
</html>