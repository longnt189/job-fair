function checkLogin(loginData) {
    $.ajax({
        url: '../../models/login.php',
        type: 'POST',
        dataType: 'json',
        data: {
            username: loginData[0],
            password: loginData[1]
        },
        success: function (data) {
            if (data === true) {
                window.location="views/admin";
            } else {
                $('#login_err').prop('hidden',false);
            }
        },
        error: function (xhr) {
            console.log('Error ' + xhr.status + ': ' +xhr.statusText + ', ' + xhr.responseText);
        }

    });
}

$(document).ready(function () {
   $('#loginButton').click(function (event) {
       event.preventDefault();

       const username = $('#username').val();
       const password = $('#password').val();

       const data = [username, password];
       console.log(data);
       checkLogin(data);
   })
});