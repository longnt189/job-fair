<?php
require '../config.php';
include ('db.php');
global $conn;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $title = isset($_POST['title']) ? $_POST['title'] : '';

    $lastName = isset($_POST['lastName']) ? $_POST['lastName'] : false;
    $firstName = isset($_POST['firstName']) ? $_POST['firstName'] : false;
    $birthday = isset($_POST['birthday']) ? $_POST['birthday'] : false;
    $email = isset($_POST['email']) ? $_POST['email'] : false;
    $phoneNumber = isset($_POST['phoneNumber']) ? $_POST['phoneNumber'] : false;
    $uni = isset($_POST['uni']) ? $_POST['uni'] : false;
    $major = isset($_POST['major']) ? $_POST['major'] : false;
    $uniYear = isset($_POST['uniYear']) ? $_POST['uniYear'] : false;
    $timeOption = isset($_POST['timeOption']) ? $_POST['timeOption'] : false;
    $interestField = isset($_POST['interestField']) ?  implode(',<br> ', $_POST['interestField']) : false;

    $fileName = isset($_FILES['fileCV']['name']) ? $_FILES['fileCV']['name'] : '';
    $tmpName = $_FILES['fileCV']['tmp_name'];
    $fileSize = $_FILES['fileCV']['size'];
    $fileType = $_FILES['fileCV']['type'];

    $errors = [];
    $location = '../upload/';
    $extension = substr($fileName, strpos($fileName, '.') + 1);

    $data = [$lastName, $firstName, $birthday, $email, $phoneNumber, $uni, $major, $uniYear, $timeOption, $interestField, $fileName, $fileType, $location];

    if ($lastName && $firstName && $birthday && $email && $phoneNumber && $uni && $major && $uniYear && $timeOption && $interestField) {

        $uploadLocation = $location.date('d-m-Y_hia').'-'.$fileName;
        move_uploaded_file($tmpName, $uploadLocation);
        $uploadLocation = '../'.$uploadLocation;

        try {
            $sql2 = "INSERT INTO file_cv (`file_name`, `file_size`, `location`) VALUES ('$fileName', '$fileSize', '$uploadLocation')";

            $conn->exec($sql2);

            $fileId = $conn->lastInsertId();

            try {
                $sql1 = "INSERT INTO cv_info (`last_name`, `first_name`, `birthday`, `email`,`phone_number`, `uni`, `major`, `uni_year`, `time_option`, `interest_field`, `file_id`) VALUES   ('$lastName', '$firstName', '$birthday', '$email', '$phoneNumber','$uni', '$major', '$uniYear', '$timeOption', '$interestField', '$fileId')";
                $conn->exec($sql1);
            } catch (PDOException $e) {
                echo $sql1 . "<br>" . $e->getMessage();
            }

            header('location: ../thong-bao');

            return true;
        } catch (PDOException $e) {
            echo $sql2 . "<br>" . $e->getMessage();
        }

        $conn = NULL;

    } else {
        return false;
    }
}
?>