<?php

function loadCV() {
    global $conn;

    try {
        $getCount = $conn->prepare("SELECT COUNT(*) FROM cv_info");
        $getCount->execute();
        $count = $getCount->fetch();
        echo 'Tổng số đơn nhận được: '.$count[0];

        try {
            $getPost = $conn->prepare('SELECT a.*, b.file_name, b.location FROM cv_info AS a  LEFT JOIN file_cv AS b ON a.file_id = b.file_id');
            $getPost->execute();
            $i = 1;
            while ($post = $getPost->fetch(PDO::FETCH_ASSOC)) :?>
                <tr>
                    <td><?php echo $i;?></td>
                    <td><?php echo $post['last_name'].' '.$post['first_name'] ?></td>
                    <td><?php echo $post['birthday'];?></td>
                    <td><?php echo $post['email']?></td>
                    <td><?php echo $post['phone_number'];?></td>
                    <td><?php echo $post['uni'];?></td>
                    <td><?php echo $post['major'];?></td>
                    <td><?php echo $post['uni_year'];?></td>
                    <td><?php echo $post['time_option'];?></td>
                    <td><?php echo $post['interest_field'];?></td>
                    <td><a href="<?php echo $post['location'];?>" target="_blank"><?php echo $post['file_name'];?></a></td>
                </tr>
                <?php
                $i ++;
            endwhile;
        } catch (PDOException $e) {
            echo '<br>'.$e->getMessage();
        }
    } catch (PDOException $e) {
        echo '<br>'.$e->getMessage();
    }
}
?>