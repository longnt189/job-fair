-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th6 18, 2018 lúc 10:25 AM
-- Phiên bản máy phục vụ: 10.1.28-MariaDB
-- Phiên bản PHP: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `jobfair`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `cv_info`
--

CREATE TABLE `cv_info` (
  `cv_id` int(11) NOT NULL,
  `last_name` varchar(70) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(70) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uni` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `major` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uni_year` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_option` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `interest_field` mediumtext COLLATE utf8mb4_unicode_ci,
  `file_id` int(11) DEFAULT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `file_cv`
--

CREATE TABLE `file_cv` (
  `file_id` int(11) NOT NULL,
  `file_name` varchar(70) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` int(11) DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_upload` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `cv_info`
--
ALTER TABLE `cv_info`
  ADD PRIMARY KEY (`cv_id`),
  ADD KEY `file_id` (`file_id`);

--
-- Chỉ mục cho bảng `file_cv`
--
ALTER TABLE `file_cv`
  ADD PRIMARY KEY (`file_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `cv_info`
--
ALTER TABLE `cv_info`
  MODIFY `cv_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `file_cv`
--
ALTER TABLE `file_cv`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `cv_info`
--
ALTER TABLE `cv_info`
  ADD CONSTRAINT `cv_info_ibfk_1` FOREIGN KEY (`file_id`) REFERENCES `file_cv` (`file_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
